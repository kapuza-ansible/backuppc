# BackupPC role
## Install
### Install python (ubuntu)
For use ansible, you need install python2.7 on remote server.
```bash
ansible mongo -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-apt"
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```

### download backuppc role
```bash
mkdir ./roles
cat <<'EOF' >> ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/backuppc.git
  name: backuppc
EOF
echo "roles/backuppc" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

## BackupPC (ubuntu)
Install and setup backuppc server.
```bash
cat << 'EOF' > backuppc.yml
---
- hosts:
    install_backuppc
  become: yes
  become_user: root
  #environment:
  #  http_proxy: http://proxy:3128
  #  https_proxy: http://proxy:3128
  tasks:
    - name: Install backuppc
      include_role:
        name: backuppc
      vars:
        bp_action: install
EOF

ansible-playbook ./backuppc.yml
```
